use info;

DROP TABLE IF EXISTS info;

CREATE TABLE info (
 id bigint(20) NOT NULL AUTO_INCREMENT,
 full_name varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
 trust_level FLoat  NOT NULL,
 PRIMARY KEY (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;