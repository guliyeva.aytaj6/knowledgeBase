let searchResult = "";
let id = null;

getProducts();

// document.getElementById('search').addEventListener('focusin', () => {
//         document.getElementById('list').style.display = 'block';
// });
//
// document.getElementById('search').addEventListener('focusout', () => {
//         document.getElementById('list').style.display = 'none';
// });

document.getElementById('search')
    .addEventListener('input', async (event) => {
        searchResult = event.target.value
        await getProducts();
});

function showResults(body) {
        document.getElementById('list').innerText = "";

        for (let i = 0; i < body.length; i++){
                const li = document.createElement('li');
                li.innerText = body[i].fullName + " " + body[i].trustLevel;
                li.value = body[i].id;
                li.addEventListener('click', () => {
                        id = body[i].id;
                });
                document.getElementById('list').appendChild(li);

        }
}

function submit(){
        console.log(id);
        if(!id){
                alert("Please select");
                return;
        }
        fetch(`http://localhost:9090/api/v1/info/select/${id}`, {
                method: 'POST',
                headers: {
                        'Content-Type': 'application/json'

                },
                body: JSON.stringify({id})
        })
document.getElementById('list').innerText = "";
}


async function getProducts(){
        const res = await fetch(`http://localhost:9090/api/v1/info?fullName=${searchResult}`, {
                method: 'GET' // http method 'GET', 'POST', 'DELETE', 'PUT',
        });
        const body = await res.json();
        showResults(body);
}


//async function getProducts(){
//        const res = await fetch(`https://dummyjson.com/products/search?q=${searchResult}`, {
//                method: 'GET' // http method 'GET', 'POST', 'DELETE', 'PUT',
//        });
//        const body = await res.json();
//        showResults(body);
//}
