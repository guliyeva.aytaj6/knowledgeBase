package infoservice.infoservice.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "info")
@NoArgsConstructor
@AllArgsConstructor
public class Info  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @JsonSerialize(using = ToStringSerializer.class)
    private String fullName;

    @JsonSerialize(using = ToStringSerializer.class)
    private float trustLevel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getTrustLevel (){
        return trustLevel;
    }

    public void  setTrustLevel (float newTrustLevel){
        this.trustLevel = newTrustLevel;
    }



}
