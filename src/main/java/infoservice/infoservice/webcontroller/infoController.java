package infoservice.infoservice.webcontroller;


import infoservice.infoservice.business.service.InfoService;
import infoservice.infoservice.model.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/info")
public class infoController {
    @Autowired
    InfoService infoService;

    @GetMapping
    public ResponseEntity<List<Info>> getResult(@Param("fullName") String fullName) {
        List<Info> searchList = infoService.getSearchResult(fullName);
        return ResponseEntity.ok(searchList);

    }

    @PostMapping("/select/{id}")
    public void selectPerson(@PathVariable("id") Long id) {
        infoService.selectById(id);
    }
}
