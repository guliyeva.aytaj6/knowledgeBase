package infoservice.infoservice.business.repository;

import infoservice.infoservice.model.Info;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@EnableJpaRepositories
@Repository
public interface InfoRepository extends JpaRepository<Info, Long> {


    List<Info> findByFullNameContains(String fullName);

    List<Info> findByIdNot(Long id);
}
