package infoservice.infoservice.business.repository.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class InfoRepositoryDTO {


    private Long id;


    private String fullName;


    private float trustLevel;

}
