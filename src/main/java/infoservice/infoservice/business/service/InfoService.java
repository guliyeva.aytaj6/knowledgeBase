package infoservice.infoservice.business.service;

import infoservice.infoservice.model.Info;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InfoService {

    List<Info> getSearchResult(String fullName);



    void selectById(Long id);
}
