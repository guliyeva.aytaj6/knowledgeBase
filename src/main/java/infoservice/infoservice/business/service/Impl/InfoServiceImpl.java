package infoservice.infoservice.business.service.Impl;

import infoservice.infoservice.business.repository.InfoRepository;
import infoservice.infoservice.business.service.InfoService;
import infoservice.infoservice.model.Info;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class InfoServiceImpl implements InfoService {

    float minLevel = 0.4f;
    float maxLevel = 1f;
    float change = 0.05f;

    @Autowired
    private InfoRepository infoRepository;

    @Override
    public List<Info> getSearchResult(String fullName) {
        List<Info> searchResult = infoRepository.findByFullNameContains(fullName);
        for (int i = 0; i < searchResult.size(); i++) {
            if (searchResult.get(i).getTrustLevel() >= maxLevel || searchResult.get(i).getTrustLevel() <= minLevel) {
                infoRepository.delete(searchResult.get(i));
                searchResult.remove(i);
            }
        }

        return searchResult.stream()
                .sorted(Comparator.comparing(Info::getTrustLevel).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public void selectById(Long id) {
        Optional<Info> personById = infoRepository.findById(id);
        List<Info> nonList = infoRepository.findByIdNot(id);
        System.out.println(nonList.get(0).getId());
        Info selectedPerson = personById.get();
        selectedPerson.setTrustLevel(selectedPerson.getTrustLevel() + (1 - selectedPerson.getTrustLevel()) * change);
        infoRepository.save(selectedPerson);
        for (Info info : nonList
        ) {
            float trust = info.getTrustLevel();
            float newTrustLevel = trust - (1 - trust) * change;
            info.setTrustLevel(newTrustLevel);
            infoRepository.save(info);
        }
    }
}


